//Caelan Whitter 1841768

package geometry;

public class LotsOfShapes {

	public static void main(String[] args) {
		Shape[] shapes = new Shape[5];
		
		shapes[0]=new Rectangle(10,3);
		shapes[1]=new Rectangle(13,7);
		shapes[2]=new Circle(6);
		shapes[3]=new Circle(20);
		shapes[4]=new Square(10);
		
		for(int i=0; i<shapes.length ; i++)
		{
			System.out.println("Area: "+shapes[i].getArea());
			System.out.println("Perimeter: "+shapes[i].getPerimeter());

		}

		
		
		
		
		
	}

}
