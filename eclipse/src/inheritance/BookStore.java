//Caelan Whitter 1841768

package inheritance;

public class BookStore {

	public static void main(String[] args) {
		
		Book [] books = new Book[5];
		
		books[0]=new Book("The Hobbit","J. R. R. Tolkien");
		books[1]=new ElectronicBook("The Little Prince","Antoine de Saint-Exupery",2600);
		books[2]=new Book("The Da Vinci Code","Dan Brown");
		books[3]=new ElectronicBook("The Alchemist","Paulo Coelho",4000);
		books[4]=new ElectronicBook("Lolita","Vladimir Nabokov",3500);
		
		for(int i=0; i<books.length ; i++)
		{
			System.out.println(books[i]);
		}
	}

}
