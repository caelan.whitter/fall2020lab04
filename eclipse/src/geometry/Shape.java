//Caelan Whitter 1841768
package geometry;

public interface Shape {
	
	double getArea();
	double getPerimeter();

}
