//Caelan Whitter 1841768

package inheritance;

public class ElectronicBook extends Book {
	private double numberBytes;
	
	
	  public ElectronicBook (String title,String author,double numberBytes)
	  
	  {
		  super(title,author);
		  this.numberBytes=numberBytes;
	  }
	  
	  public String toString()
	  {
		  String fromBase = super.toString();
		  
	      return fromBase+" Number of Bytes: "+this.numberBytes;
	      
	  }

	
	

}
